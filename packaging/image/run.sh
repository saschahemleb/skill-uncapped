#!/usr/bin/env bash

set -eu

function usage() {
    cat <<USAGE

    Usage: $0 [-t target]

    Options:
        -t: which target to run. Available targets are "web", "queue" and "schedule"
USAGE
    exit 1
}

if [ $# -eq 0 ]; then
    usage
    exit 1
fi

TARGET=

while [ "${1:-}" != "" ]; do
    case $1 in
    -t)
        shift
        TARGET=$1
        ;;
    *)
        shift
        ;;
    esac
    shift
done

if [[ $TARGET == "" ]]; then
    echo "You must provide a target"
    exit 1
fi

gosu www-data php /var/www/html/artisan config:cache
gosu www-data php /var/www/html/artisan route:cache
gosu www-data php /var/www/html/artisan storage:link

if [[ $TARGET == "web" ]]; then
    exec apache2-foreground
elif [[ $TARGET == "queue" ]]; then
    exec gosu www-data php /var/www/html/artisan horizon
elif [[ $TARGET == "schedule" ]]; then
    exec gosu www-data php /var/www/html/artisan schedule:work
else
    echo "You must provide a valid target"
    exit 1
fi
