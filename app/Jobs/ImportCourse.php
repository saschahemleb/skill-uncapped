<?php

namespace App\Jobs;

use App\Models\Course;
use App\Models\Episode;
use App\Structs\Dump\Video;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;

class ImportCourse implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @param Video[] $videos
     */
    public function __construct(private \App\Structs\Dump\Course $dumpCourse, private array $videos)
    {
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $course = $this->handleCourse();
        $this->handleEpisodes($course);
    }

    private function handleCourse(): Course
    {
        /** @var Course $course */
        $course = Course::query()->firstWhere('skillcapped_id', $this->dumpCourse->getUuid());
        if ($course === null) {
            $course = Course::query()->make();
        }
        $course->fill(
            [
                'skillcapped_id' => $this->dumpCourse->getUuid(),
                'name' => $this->dumpCourse->getTitle(),
                'category' => $this->dumpCourse->getRole(),
            ]
        )->save();

        if (
            !empty($this->dumpCourse->getCourseImage()) &&
            !Storage::exists($course->thumbnail_path)) {
            Storage::put($course->thumbnail_path, Http::get($this->dumpCourse->getCourseImage()));
        }

        return $course;
    }

    private function handleEpisodes(Course $course): void
    {
        foreach ($this->videos as $coursePlaylistPositionIndex => $video) {
            /** @var Episode $episode */
            $episode = $course->episodes->firstWhere('skillcapped_id', $video->getUuid());
            if ($episode === null) {
                $episode = $course->episodes()->make();
            }

            $episode->fill([
                'skillcapped_id' => $video->getUuid(),
                'course_playlist_position_index' => $coursePlaylistPositionIndex,
                'name' => $video->getTitle(),
                'length' => $video->getDurSec(),
                'description' => $video->getDesc(),
                'outdated_reason' => $video->getOdr(),
            ])->save();

            if (!Storage::exists($episode->thumbnail_path)) {
                $thumbnailUrl = $video->getTSS() ?: "https://ik.imagekit.io/skillcapped/thumbnails/{$episode->skillcapped_id}/thumbnails/thumbnail_10.jpg?tr=w-1400";
                Storage::put($episode->thumbnail_path, Http::get($thumbnailUrl));
            }
        }
    }
}
