<?php

namespace App\Jobs;

use App\Models\Course;
use App\Models\Episode;
use App\Structs\CourseDump;
use App\Structs\Dump\Dump;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class ScrapeSkillCapped implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $dump = $this->fetchDump();

        foreach ($dump->getCourses() as $dumpCourse) {
            ImportCourse::dispatch($dumpCourse, $dump->getVideosForCourse($dumpCourse));
        }
    }

    private function fetchDump(): Dump
    {
        $courseDumpUrl = Str::of(
            Http::get('https://www.skill-capped.com/wow/browse')->body()
        )->match('(https:\/\/lol-content-dumps\.s3\.amazonaws\.com\/courses_v2\/wow\/course_dump_\d+\.json)');
        return Dump::fromArray(Http::get($courseDumpUrl)->json());
    }
}
