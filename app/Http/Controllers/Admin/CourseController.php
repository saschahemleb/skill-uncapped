<?php

declare(strict_types=1);

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller as BaseController;
use App\Models\Course;
use Inertia\Inertia;
use Inertia\Response;

class CourseController extends BaseController
{
    public function index(): Response
    {
        return Inertia::render('Admin/Course/List', [
            'courses' => Course::query()->with('episodes')->get(),
        ]);
    }

    public function show(Course $course): Response
    {
        return Inertia::render('Admin/Course/Show', [
            'course' => $course,
        ]);
    }
}
