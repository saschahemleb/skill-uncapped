<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Course extends Model
{
    use HasFactory;

    protected $appends = [
        'name_slugged',
        'thumbnail_url',
    ];

    protected $fillable = [
        'skillcapped_id',
        'name',
        'category',
    ];

    public function episodes(): HasMany
    {
        return $this->hasMany(Episode::class)->orderBy('course_playlist_position_index');
    }

    public function getNameSluggedAttribute(): string
    {
        return Str::slug($this->attributes['name']);
    }

    public function getThumbnailPathAttribute(): string
    {
        return "courses/{$this->getAttribute('skillcapped_id')}.png";
    }

    public function getThumbnailUrlAttribute(): string
    {
        return Storage::url($this->getAttribute('thumbnail_path'));
    }
}
