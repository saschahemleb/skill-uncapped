<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Episode extends Model
{
    use HasFactory;

    protected $appends = [
        'name_slugged',
        'thumbnail_url',
    ];

    protected $fillable = [
        'skillcapped_id',
        'course_playlist_position_index',
        'name',
        'length',
        'description',
        'outdated_reason',
    ];

    public function course(): BelongsTo
    {
        return $this->belongsTo(Course::class);
    }

    public function getNameSluggedAttribute(): string
    {
        return Str::slug($this->attributes['name']);
    }

    public function getM3u8Attribute(): string
    {
        $fullChunksCount = floor($this->getAttribute('length') / 10);
        $lastChunkLength = $this->getAttribute('length') % 10;

        $extChunks = [];
        if ($lastChunkLength > 0) {
            $tsId = str_pad($fullChunksCount+1, 5, '0', STR_PAD_LEFT);
            $extChunks[] = "#EXTINF:{$lastChunkLength}\nhttps://d13z5uuzt1wkbz.cloudfront.net/{$this->getAttribute('skillcapped_id')}/HIDDEN4500-{$tsId}.ts";
        }
        for(; $fullChunksCount > 0; $fullChunksCount--) {
            $tsId = str_pad($fullChunksCount, 5, '0', STR_PAD_LEFT);
            $extChunks[] = "#EXTINF:10\nhttps://d13z5uuzt1wkbz.cloudfront.net/{$this->getAttribute('skillcapped_id')}/HIDDEN4500-{$tsId}.ts";
        }

        $chunks = implode("\n", array_reverse($extChunks));
        return <<<m3u8
#EXTM3U
#EXT-X-PLAYLIST-TYPE:VOD
#EXT-X-TARGETDURATION:10
{$chunks}
#EXT-X-ENDLIST
m3u8;
    }

    public function getThumbnailPathAttribute(): string
    {
        return "episodes/{$this->getAttribute('skillcapped_id')}.png";
    }

    public function getThumbnailUrlAttribute(): string
    {
        return Storage::url($this->getAttribute('thumbnail_path'));
    }
}
