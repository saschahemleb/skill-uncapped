<?php

namespace App\Console\Commands;

use App\Actions\CreateUser;
use Illuminate\Console\Command;

class AuthCreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth:user-create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create a new user';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(CreateUser $creator)
    {
        $input = [
            'email' => $this->ask('E-Mail'),
            'password' => $this->secret('Password'),
            'password_confirmation' => $this->secret('Confirm Password'),
        ];
        $input['name'] = $input['email'];

        $creator->create($input);

        $this->info('User successfully created');

        return 0;
    }
}
