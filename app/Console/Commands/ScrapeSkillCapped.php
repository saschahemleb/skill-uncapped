<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class ScrapeSkillCapped extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'scrape-skill-capped';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Scrapes Skill-Capped for new courses and episodes';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        \App\Jobs\ScrapeSkillCapped::dispatchSync();

        return 0;
    }
}
