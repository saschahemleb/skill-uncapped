<?php

declare(strict_types=1);

namespace App\Structs\Dump;

/**
 * @internal
 */
class Patch
{
    public function __construct(
        private string $patchVal,
        private int $releaseDate,
        private string $patchUrl,
    ) {
    }

    public static function fromArray(array $patch)
    {
        return new static(
            $patch['patchVal'],
            $patch['releaseDate'],
            $patch['patchUrl'],
        );
    }

    public function getPatchVal(): string
    {
        return $this->patchVal;
    }

    public function getReleaseDate(): int
    {
        return $this->releaseDate;
    }

    public function getPatchUrl(): string
    {
        return $this->patchUrl;
    }
}
