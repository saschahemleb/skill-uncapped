<?php

declare(strict_types=1);

namespace App\Structs\Dump;

/**
 * @internal
 */
class Video
{
    public function __construct(
        private string $role,
        private string $title,
        private string $desc,
        private int $rDate,
        private int $durSec,
        private string $uuid,
        private int $tId,
        private string $tSS,
        private string $cSS,
        private ?string $od = null,
        private ?string $odr = null,
    ) {}

    public static function fromArray(array $data)
    {
        return new static(
            $data['role'],
            $data['title'],
            $data['desc'],
            $data['rDate'],
            $data['durSec'],
            $data['uuid'],
            $data['tId'],
            $data['tSS'],
            $data['cSS'],
            $data['od'] ?? null,
            $data['odr'] ?? null,
        );
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getDesc(): string
    {
        return $this->desc;
    }

    public function getRDate(): int
    {
        return $this->rDate;
    }

    public function getDurSec(): int
    {
        return $this->durSec;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getTId(): int
    {
        return $this->tId;
    }

    public function getTSS(): string
    {
        return $this->tSS;
    }

    public function getCSS(): string
    {
        return $this->cSS;
    }

    public function getOd(): ?string
    {
        return $this->od;
    }

    public function getOdr(): ?string
    {
        return $this->odr;
    }
}
