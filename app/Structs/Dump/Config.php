<?php

declare(strict_types=1);

namespace App\Structs\Dump;

/**
 * @internal
 */
class Config
{
    public function __construct(
        private string $game,
        private string $tcoaching,
    ) {
    }

    public static function fromArray(array $config)
    {
        return new static(
            $config['game'],
            $config['tcoaching'],
        );
    }

    public function getGame(): string
    {
        return $this->game;
    }

    public function getTcoaching(): string
    {
        return $this->tcoaching;
    }
}
