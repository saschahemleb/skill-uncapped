<?php

declare(strict_types=1);

namespace App\Structs\Dump;

/**
 * @internal
 */
class Course
{
    public function __construct(
        private string $title,
        private string $uuid,
        private string $desc,
        private int $rDate,
        private string $role,
        private string $courseImage,
        private string $courseImage2,
    ) {
    }

    public static function fromArray(array $data)
    {
        return new static(
            $data['title'],
            $data['uuid'],
            $data['desc'],
            $data['rDate'],
            $data['role'],
            $data['courseImage'],
            $data['courseImage2'],
        );
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getUuid(): string
    {
        return $this->uuid;
    }

    public function getDesc(): string
    {
        return $this->desc;
    }

    public function getRDate(): int
    {
        return $this->rDate;
    }

    public function getRole(): string
    {
        return $this->role;
    }

    public function getCourseImage(): string
    {
        return $this->courseImage;
    }

    public function getCourseImage2(): string
    {
        return $this->courseImage2;
    }
}
