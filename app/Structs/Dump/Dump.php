<?php

declare(strict_types=1);

namespace App\Structs\Dump;

class Dump
{
    public static function fromArray(array $input)
    {
        return new static(
            $input['timeStamp'],
            Patch::fromArray($input['patch']),
            Config::fromArray($input['config']),
            array_map(fn($data) => Video::fromArray($data), $input['videos']),
            array_map(fn($data) => Course::fromArray($data), $input['courses']),
            $input['videosToCourses'],
        );
    }

    /**
     * @param Video[] $videos
     * @param Course[] $courses
     */
    private function __construct(
        private int $timeStamp,
        private Patch $patch,
        private Config $config,
        private array $videos,
        private array $courses,
        private array $videosToCourses
    ) {
    }

    public function getTimeStamp(): int
    {
        return $this->timeStamp;
    }

    public function getPatch(): Patch
    {
        return $this->patch;
    }

    public function getConfig(): Config
    {
        return $this->config;
    }

    /**
     * @return Video[]
     */
    public function getVideos(): array
    {
        return $this->videos;
    }

    /**
     * @return Course[]
     */
    public function getCourses(): array
    {
        return $this->courses;
    }

    /**
     * @return Video[]
     */
    public function getVideosForCourse(Course $course): array
    {
        $title = $course->getTitle();

        $vtc = collect(($this->videosToCourses[$title]??[])['chapters'] ?? []);
        if ($vtc->count() === 0) {
            return [];
        }

        $videoUuids = $vtc->pluck('vids')->flatten()->all();

        $videos = $this->getVideos();

        return array_filter(array_map(function($vidUuid) use ($videos) {
            foreach ($videos as $video) {
                if ($video->getUuid() === $vidUuid) {
                    return $video;
                }
            }

            return null;
        }, $videoUuids));
    }
}
