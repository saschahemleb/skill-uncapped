<?php

use App\Models\Course;
use App\Models\Episode;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;
use Symfony\Component\HttpFoundation\Response;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/healthz', fn() => \response()->json('ok'));

Route::get('/', function () {
    return Inertia::render('Welcome', [
        'assetBaseUrl' => asset('/'),
        'coursesPerPage' => 9,
        'categories' => Cache::remember('categories', now()->addMinute(), fn() => Course::query()->select('category')->distinct()->get()->pluck('category')),
        'courses' => Cache::remember('courses', now()->addMinute(), fn() => Course::query()->with('episodes')->get()),
    ]);
});

Route::get('/wow/course/{course_name}/{course_id}', function ($courseName, $courseId) {
    $course = Course::query()->where('skillcapped_id', $courseId)->firstOrFail();
    $firstEpisode = $course->episodes->first();
    if ($firstEpisode === null) {
        return redirect('/');
    }

    return to_route('episode_canonical.show', [
        'course_name' => $course->name_slugged,
        'course_id' => $course->skillcapped_id,
        'episode_name' => $firstEpisode->name_slugged,
        'episode_id' => $firstEpisode->skillcapped_id,
    ], Response::HTTP_MOVED_PERMANENTLY);
})->name('course_canonical.show');

Route::get('/wow/course/{course_name}/{course_id}/{episode_name}/{episode_id}', function ($courseName, $courseId, $episodeName, $episodeId) {
    $episode = Episode::query()->with('course.episodes')->where('skillcapped_id', $episodeId)->firstOrFail();

    return Inertia::render('Episode/Show', [
        'assetBaseUrl' => asset('/'),
        'episode' => $episode,
    ]);
})->name('episode_canonical.show');

Route::get('/{episode_id}.m3u8', function ($episodeId) {
    $m3u8 = Cache::remember("m3u8_{$episodeId}", now()->addHour(), fn() => Episode::query()->where('skillcapped_id', $episodeId)->firstOrFail()->m3u8);

    return \response($m3u8);
});

require __DIR__.'/auth.php';
