# skill-uncapped

skill-uncapped is a web-app to browse and watch courses from [skill-capped](https://www.skill-capped.com/wow/landing) free of charge.

## How it works

## Architecture

Inspired by an elitepvpers comment ([here](https://www.elitepvpers.com/forum/wow-guides-templates/4314622-skill-capped-videos-free-4.html#post38560829)), this application's
job is to detect the amount of chunks for each lesson and serve the .m3u8 file to clients.
No video hosting is done here, just a more intuitive UI for the little workaround described in the forum post.

The application is split into the following parts:

### Crawler

The crawler will periodically scrape skill-cappeds browse section to discover new courses or detect courses that have been removed.
Additionally, it will fetch all the episodes of a course and probe for the amount of chunks for every episode.

### API

The API is used by the web ui and exposes routes to list courses and episodes and to fetch to pre-built .m3u8 file.

### Web UI

The web ui is a SPA, talking to the API and playing course episodes to the users.

## Set it up

Clone the project, install packages with `composer install` and run the application with `vendor/bin/sail up`.
