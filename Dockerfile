FROM node:18.0-bullseye as frontend
WORKDIR /app/
COPY package.json package-lock.json /app/
RUN set -eux; \
    npm ci

COPY . /app/
RUN npm run prod

FROM composer:2.0 as backend
COPY composer.json composer.lock /app/
RUN set -eux; \
    composer install --prefer-dist --no-dev --ignore-platform-reqs --no-autoloader --no-interaction --no-scripts

COPY . /app/
RUN set -eux; \
    composer dump-autoload --optimize

FROM php:8.1-apache-bullseye as production

RUN set -eux; \
    apt-get update; \
    apt-get install -y gosu ca-certificates;

RUN set -eux; \
    docker-php-ext-configure opcache --enable-opcache; \
    docker-php-ext-install pdo pdo_mysql; \
    docker-php-ext-install pcntl; \
    pecl install redis; docker-php-ext-enable redis;
COPY packaging/image/php/conf.d/opcache.ini /usr/local/etc/php/conf.d/opcache.ini

RUN set -eux; \
    a2enmod rewrite
COPY packaging/image/apache2/sites-available/000-default.conf /etc/apache2/sites-available/000-default.conf

COPY --from=frontend --chown=www-data:www-data /app/ /var/www/html
COPY --from=backend --chown=www-data:www-data /app/ /var/www/html
ENV APP_ENV=production
ENV APP_DEBUG=false

ENTRYPOINT ["/var/www/html/packaging/image/run.sh"]
CMD ["-t", "web"]
