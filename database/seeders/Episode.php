<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class Episode extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Course::query()->each(function(\App\Models\Course $course) {
            \App\Models\Episode::factory(5)->afterMaking(function(\App\Models\Episode $episode) use ($course) {
                $episode->course()->associate($course);
            })->create();
        });
    }
}
