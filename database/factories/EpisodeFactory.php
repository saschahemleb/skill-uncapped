<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Episode>
 */
class EpisodeFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'skillcapped_id' => $this->faker->lexify('??????????'),
            'course_playlist_position_index' => $this->faker->randomDigitNotNull(),
            'name' => $this->faker->name(),
            'length' => $this->faker->numberBetween(1, 500),
        ];
    }
}
