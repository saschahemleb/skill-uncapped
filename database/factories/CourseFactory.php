<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Course>
 */
class CourseFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'skillcapped_id' => $this->faker->lexify('??????????'),
            'name' => $this->faker->name,
            'category' => $this->faker->randomElement([
                'General',
                'DK', 'DH', 'Druid', 'Hunter', 'Mage', 'Monk', 'Paladin', 'Priest', 'Rogue', 'Shaman', 'Warlock', 'Warrior',
                'BGs',
                'TBC',
            ])
        ];
    }
}
